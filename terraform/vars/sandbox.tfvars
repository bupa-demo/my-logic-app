#logic app plan
subscription_id               = "2f668271-ed2b-460d-ae05-baa23ebd3eec"
app_service_plan_name         = "sandbox-asp-01"
location                      = "westeurope"
resource_group_name           = "rg-logicapp-sandbox"
default_tags                  = {
"Billing"    = "BUKMU-77581 NonProd"
"IT Service" = "BusinessApp, Teams sites, Zero Attachment"
"Owner ID"   = ""
"Project"    = ""
"ENV"        = "NFT"
}
os_type                       = "Linux"
sku_name                      = "B1"
worker_count                  = "1"
maximum_elastic_worker_count  = "2"
per_site_scaling_enabled      = "0"

#storage account
storage_name                        = "sasandboxlogicappdemo"
replication_type                    = "LRS"
account_tier                        = "Standard"
enable_https_traffic_only           = true
infrastructure_encryption_enabled   = true
allow_nested_items_to_be_public     = false
shared_access_key_enabled           = true
default_to_oauth_authentication     = true
min_tls_version                     = "TLS1_2"
enable_hns                          = false
nfsv3_enabled                       = false
cross_tenant_replication_enabled    = true
access_tier                         = "Hot"
container_delete_retention_days     = 30
blob_versioning_enabled             = true
blob_delete_retention_days          = 30
account_kind                        = "StorageV2"
change_feed_enabled                 = false
encryption_scopes                   = {}
default_network_rule                = "Deny"

#virtual network
vnet_name       = "VNET-sandbox-logicapp-demo"
vnet_cidr       = ["10.10.136.0/24"]
dns_servers	    = ["10.10.127.4", "10.10.127.5"]

#private endpoint
private_endpoint_name               = "PE-sandbox-logic-app-demo"
subresource_names                   = ["blob"]
private_dns_zone                    = "privatelink.blob.core.windows.net"
private_endpoint_name_vnl           = "VNL-sandbox-logic-app-demo"

#Route Table 
logic_app_route_table_name      = "RT-cose-euw-TrustNonProdSpoke-SharePoint-SHD-Smoke-AppGw"
enable_force_tunneling          = true
disable_bgp_route_propagation   = false
logic_app_route_name = [
    {
    name                        = "ROUTE-10_0_0_0-Via-TrustPAFWLoadBalancer"
    address_prefix              = "10.0.0.0/8"
    next_hop_type               = "VirtualAppliance"
    next_hop_in_ip_address      = "10.188.0.4"
  },
  {
    name                        = "ROUTE-172_16_0_0-Via-TrustPAFWLoadBalancer"
    address_prefix              = "172.16.0.0/12"
    next_hop_type               = "VirtualAppliance"
    next_hop_in_ip_address      = "10.188.0.4"
  },
  {
    name                        = "ROUTE-192_168_0_0-Via-TrustPAFWLoadBalancer"
    address_prefix              = "192.168.0.0/16"
    next_hop_type               = "VirtualAppliance"
    next_hop_in_ip_address      = "10.188.0.4"
  },
  {
    name                        = "ROUTE-Default-Via-TrustPAFWLoadBalancer"
    address_prefix              = "0.0.0.0/0"
    next_hop_type               = "VirtualAppliance"
    next_hop_in_ip_address      = "10.188.0.4"
  },
  {
    name                        = "ROUTE-eunintpafw01-Via-Direct"
    address_prefix              = "10.188.0.5/32"
    next_hop_type               = "VirtualAppliance"
    next_hop_in_ip_address      = "10.188.0.5"
  },
  {
    name                        = "ROUTE-eunintpafw02-Via-Direct"
    address_prefix              = "10.188.0.6/32"
    next_hop_type               = "VirtualAppliance"
    next_hop_in_ip_address      = "10.188.0.6"
  },
  {
    name                        = "ROUTE-SemiTrustHubVNet-Via-SemiTrustPAFWLoadBalancer"
    address_prefix              = "10.188.32.0/20"
    next_hop_type               = "VirtualAppliance"
    next_hop_in_ip_address      = "10.188.32.4"
  },
  {
    name                        = "ROUTE-SpokeVNet-Via-TrustPAFWLoadBalancer"
    address_prefix              = "10.189.160.0/21"
    next_hop_type               = "VirtualAppliance"
    next_hop_in_ip_address      = "10.188.0.4"
  },
  {
    name                        = "ROUTE-subnet-Via-VirtualNetwork"
    address_prefix              = "10.189.169.32/27"
    next_hop_type               = "VnetLocal"
    next_hop_in_ip_address      = null
  },
  {
    name                        = "ROUTE-TrustHubVNet-Via-TrustPAFWLoadBalancer"
    address_prefix              = "10.188.0.0/20"
    next_hop_type               = "VirtualAppliance"
    next_hop_in_ip_address      = "10.188.0.4"
  }
]

#Network Security Group
logic_app_network_security_group_name = "NSG-sandbox-logic-app-demo"

#Subnet
logic_app_subnet_name           = "SNET-sandbox-logic-app-demo"
logic_app_subnet_cidr_list      = ["10.10.136.0/24"]


